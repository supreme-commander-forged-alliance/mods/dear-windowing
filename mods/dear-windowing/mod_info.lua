name = "Dear Windowing"
version = 9
uid = "dear-windowing-09"

icon = "/mods/dear-windowing/icon.png"
description = "An intuitive approach to creating UI elements for simple UI mods or debugging your own mod."
author = "(Jip) Willem Wijnia"
exclusive = false
ui_only = true